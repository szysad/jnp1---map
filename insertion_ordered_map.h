// created by rm406247 + ss406325

#ifndef JNP1___MAP_INSERTION_ORDERED_MAP_H
#define JNP1___MAP_INSERTION_ORDERED_MAP_H

#include <functional>
#include <list>
#include <unordered_map>
#include <memory>

class lookup_error : public std::exception {
    const char* what() const noexcept {
        return "lookup_error";
    }
};

template <class K, class V, class Hash = std::hash<K>>
class insertion_ordered_map {
    using insertion_order_t = typename std::pair<K, V>;
    using list_reference = typename std::list<insertion_order_t>::iterator;

    struct pointer_hash {
        const Hash hash;
        size_t operator()(const K * const &k) const {
            return hash(*k);
        }
    };

    struct pointer_equal {
        bool operator()(const K * const &a, const K * const &b) const {
            return *a == *b;
        }
    };

    struct dict {
        std::unordered_map<K const * const, list_reference, pointer_hash, pointer_equal> map;
        std::list<insertion_order_t> insertion_order;
        dict() = default;
        dict(dict const &);
    };

    std::shared_ptr<dict> map_pointer;
    bool poisoned;

    class modification_guard {
        insertion_ordered_map *map_p;
        bool succeeded;
        std::shared_ptr<dict> before;

    public:
        modification_guard(insertion_ordered_map *map) {
            map_p = map;
            succeeded = true;
            if (!map->map_pointer.unique()) {
                before = map->map_pointer;
                map->map_pointer = std::make_shared<dict>(*map->map_pointer);
                succeeded = false;
            }
        }

        ~modification_guard() {
            if (!succeeded) {
                map_p->map_pointer = before;
            }
        }

        void success() {
            succeeded = true;
        }
    };

    void swap(insertion_ordered_map &m1, insertion_ordered_map &m2);

public:
    using iterator = typename std::list<insertion_order_t>::const_iterator;

    insertion_ordered_map();

    insertion_ordered_map(insertion_ordered_map const &other);

    insertion_ordered_map(insertion_ordered_map &&other) noexcept = default;

    ~insertion_ordered_map() noexcept;

    insertion_ordered_map &operator=(insertion_ordered_map other);

    bool insert(K const &k, V const &v);

    void erase(K const &k);

    void merge(insertion_ordered_map const &other);

    V &at(K const &k);

    V const &at(K const &k) const;

    V &operator[](K const &k);

    size_t size() const noexcept;

    bool empty() const noexcept;

    void clear();

    bool contains(K const &k) const;

    iterator begin() const;

    iterator end() const;

};

template <class K, class V, class Hash>
insertion_ordered_map<K, V, Hash>::dict::dict(dict const &other) {
    insertion_order = other.insertion_order;
    for (list_reference it = insertion_order.begin(), end = insertion_order.end(); it != end; it++) {
        map.insert({&it->first, it});
    }
}

template <class K, class V, class Hash>
insertion_ordered_map<K, V, Hash>::insertion_ordered_map() : map_pointer(std::make_shared<dict>()), poisoned(false) {}

template <class K, class V, class Hash>
insertion_ordered_map<K, V, Hash>::insertion_ordered_map(insertion_ordered_map<K, V, Hash> const &other) {
    poisoned = false;
    if (other.poisoned) {
        map_pointer = std::make_shared<dict>(*other.map_pointer);
    } else {
        map_pointer = other.map_pointer;
    }
}

template <class K, class V, class Hash>
void insertion_ordered_map<K, V, Hash>::swap(insertion_ordered_map<K, V, Hash> &m1, insertion_ordered_map<K, V, Hash> &m2) {
    std::swap(m1.map_pointer, m2.map_pointer);
    std::swap(m1.poisoned, m2.poisoned);
}

template <class K, class V, class Hash>
insertion_ordered_map<K, V, Hash>& insertion_ordered_map<K, V, Hash>::operator=(insertion_ordered_map<K, V, Hash> other) {
    swap(*this, other);
    return *this;
}

template <class K, class V, class Hash>
insertion_ordered_map<K, V, Hash>::~insertion_ordered_map() noexcept = default;

template <class K, class V, class Hash>
bool insertion_ordered_map<K, V, Hash>::insert(K const &k, V const &v) {
    modification_guard guard(this);
    auto find_res = map_pointer->map.find(&k); // May throw but it's ok.

    if (find_res == map_pointer->map.end()) {
        map_pointer->insertion_order.emplace_back(k, v); // May throw but it's ok.
        list_reference list_ref = --map_pointer->insertion_order.end();

        try {
            map_pointer->map.emplace(&list_ref->first, list_ref);
        } catch (...) {
            map_pointer->insertion_order.erase(list_ref); // If it did throw we revert changes.
            throw;
        }
        poisoned = false;
        guard.success();
        return true;
    } else {
        map_pointer->insertion_order.splice(
                map_pointer->insertion_order.end(),
                map_pointer->insertion_order,
                find_res->second);
        poisoned = false;
        guard.success();
        return false;
    }
}

template<class K, class V, class Hash>
bool insertion_ordered_map<K, V, Hash>::empty() const noexcept {
    return map_pointer->map.empty();
}

template<class K, class V, class Hash>
bool insertion_ordered_map<K, V, Hash>::contains(const K &k) const {
    auto find_res = map_pointer->map.find(&k); // May throw but it's ok.
    return find_res != map_pointer->map.end();
}

template<class K, class V, class Hash>
size_t insertion_ordered_map<K, V, Hash>::size() const noexcept {
    return map_pointer->map.size();
}

template<class K, class V, class Hash>
void insertion_ordered_map<K, V, Hash>::erase(const K &k) {
    auto find_res = map_pointer->map.find(&k);
    if (find_res == map_pointer->map.end()) throw lookup_error();
    modification_guard guard(this);
    map_pointer->insertion_order.erase(find_res->second);
    map_pointer->map.erase(find_res);
    poisoned = false;
    guard.success();
}

template<class K, class V, class Hash>
void insertion_ordered_map<K, V, Hash>::merge(insertion_ordered_map const &other) {
    modification_guard guard(this);

    dict d(*map_pointer);
    // If anything throws here it is ok since we are working on a copy.
    for (auto it = other.map_pointer->insertion_order.begin(),
            end = other.map_pointer->insertion_order.end();
            it != end;
            it++) {
        auto find_res = d.map.find(&it->first);
        if (find_res != d.map.end()) {
            d.insertion_order.splice(
                    d.insertion_order.end(),
                    d.insertion_order,
                    find_res->second);
        }
        else {
            d.insertion_order.emplace_back(it->first, it->second);
            list_reference last = --d.insertion_order.end();
            d.map.emplace(&last->first, last);
        }
    }
    map_pointer = std::make_shared<dict>(d);
    poisoned = false;
    guard.success();
}

template<class K, class V, class Hash>
V &insertion_ordered_map<K, V, Hash>::operator[](const K &k) {
    modification_guard guard(this);

    auto find_res = map_pointer->map.find(&k); // May throw but it's ok.
    if (find_res == map_pointer->map.end()) {
        insert(k, V());
        poisoned = true;
        guard.success();
        return (--map_pointer->insertion_order.end())->second;
    }

    poisoned = true;
    guard.success();
    return find_res->second->second;
}

template<class K, class V, class Hash>
void insertion_ordered_map<K, V, Hash>::clear() {
    if (!map_pointer.unique()) {
        map_pointer = std::make_shared<dict>(*map_pointer);
    }
    map_pointer->map.clear();
    map_pointer->insertion_order.clear();
    poisoned = false;
}

template<class K, class V, class Hash>
typename insertion_ordered_map<K, V, Hash>::iterator insertion_ordered_map<K, V, Hash>::begin() const {
    return map_pointer->insertion_order.cbegin();
}

template<class K, class V, class Hash>
typename insertion_ordered_map<K, V, Hash>::iterator insertion_ordered_map<K, V, Hash>::end() const {
    return map_pointer->insertion_order.cend();
}

template<class K, class V, class Hash>
V const &insertion_ordered_map<K, V, Hash>::at(const K &k) const {
    try {
        const list_reference rez = map_pointer->map.at(&k);
        return rez->second;
    } catch (std::out_of_range&) {
        throw lookup_error();
    }
}


template<class K, class V, class Hash>
V &insertion_ordered_map<K, V, Hash>::at(const K &k) {
    modification_guard guard(this);
    try {
        list_reference rez = map_pointer->map.at(&k);
        poisoned = true;
        guard.success();
        return rez->second;
    } catch (std::out_of_range&) {
        throw lookup_error();
    }
}

#endif //JNP1___MAP_INSERTION_ORDERED_MAP_H
